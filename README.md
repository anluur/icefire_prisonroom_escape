# README #

### Prison escape solution ###

* First read method of KeyParser class checks persons hashcode and if it 
is the same as mine, grants access to all cells for this person.
Checking hashcode is just a simple way to not display my name in code.

* Method grantAccessToAllCells takes first cell out of static map from PrisonRoom class
and uses it as an entry point to network of cells. To go over all of the cells it calls
traverseCells method.

* Then recursive method traverseCells starts to go over all cells starting from home cell
of given person. It treats cells as graph where all the cells are connected
with their list of neighbours. It uses depth-first approach. Method uses set cells and writes every traversed cell
there to know which cells it already traversed. For every cell it calls parseCell method

* parseCell method uses reflect to access private field allowedPersons of given cell.
Then it makes it accessible, copies it, overrides it's toString method so that it does not print given person's name, 
adds given person into copy and replaces original set with new modified copy of it.